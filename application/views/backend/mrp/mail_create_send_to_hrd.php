<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Remaining 7 Days Employee Contract</title>
  <style type="text/css">
  body {margin: 0; padding: 0; min-width: 100%!important;}
  img {height: auto;}
  .content {width: 100%; max-width: 900px;}
  .header {padding: 40px 30px 20px 30px;}
  .innerpadding {padding: 20px 20px 20px 20px;}
  .borderbottom {border-bottom: 1px solid #f2eeed;}
  .subhead {font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;}
  .h1, .h2, .bodycopy {color: #153643; font-family: sans-serif;}
  .h1 {font-size: 33px; line-height: 38px; font-weight: bold;}
  .h2 {padding: 0 0 15px 0; font-size: 20px; line-height: 28px; font-weight: bold;}
  .bodycopy {font-size: 16px; line-height: 22px;}
  .button {text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;}
  .button a {color: #ffffff; text-decoration: none;}
  .footer {padding: 20px 30px 15px 30px;}
  .footercopy {font-family: sans-serif; font-size: 14px; color: #ffffff;}
  .footercopy a {color: #ffffff; text-decoration: underline;}

  @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
    body[yahoo] .hide {display: none!important;}
    body[yahoo] .buttonwrapper {background-color: transparent!important;}
    body[yahoo] .button {padding: 0px!important;}
    body[yahoo] .button a {background-color: #e05443; padding: 15px 15px 13px!important;}
    body[yahoo] .unsubscribe {display: block; margin-top: 20px; padding: 10px 50px; background: #2f3942; border-radius: 5px; text-decoration: none!important; font-weight: bold;}
  }

  @media only screen and (min-device-width: 601px) {
    .content {width: 600px !important;}
    .col425 {width: 425px!important;}
    .col380 {width: 380px!important;}
  }

</style>
</head>

<body yahoo bgcolor="#f6f8f1">
  <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td>    
        <table bgcolor="#ffffff" class="content" align="center" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td bgcolor="#00e6ac" class="header">
              <table width="70" align="left" border="0" cellpadding="0" cellspacing="0">  
                <tr>
                  <td height="70" style="padding: 0 20px 20px 0;">
                    <img class="fix" src="<?php echo base_url('/src/material/global/assets/images/ramhys_icon.png');?>" width="70" height="70" border="0" alt="" />
                  </td>
                </tr>
              </table>
              <table class="col425" align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 425px;">  
                <tr>
                  <td height="70">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="subhead" style="padding: 0 0 0 3px;">
                         Remaining 7 Days Employee Contract
                       </td>
                     </tr>
                     <tr>
                      <td class="h2" style="padding: 5px 0 0 0; color: #007acc;">
                        <b>RAMHYS (Ram Hybrid System)</b>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td class="innerpadding borderbottom">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="h2" style="color: #e68a00;">
                  <!-- Request by <?=$name_req;?> To <?=$name_to;?> - <?=$approve_to;?> -->
                  Test
                </td>
              </tr>
              <tr>
                <td class="bodycopy">
                  <!-- New Annual Leave Request Code No: <b style="color: #ff0000;"><?=$code_al;?></b> -->
                  Test
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td class="innerpadding borderbottom">
            <table class="col380" align="left" border="0" cellpadding="0" cellspacing="0" style="width: 120%; max-width: 380px;">  
              <tr>
                <td>
                  <table width="130%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="bodycopy" style="color: #e68a00;">
                        <b>Description Annual Leave Request :</b>
                      </td>
                    </tr>
                    <tr>
                      <td class="bodycopy" style="padding: 5px 0 0 0;">
                        <!-- Code No : <?=$code_al;?> -->
                        Test
                      </td>
                    </tr>

                    <tr>
                      <td class="bodycopy" style="padding: 5px 0 0 0;">
                        <!-- No.Emp : <?=$employee_no;?> -->
                        Test
                      </td>
                    </tr>

                    <tr>
                      <td class="bodycopy" style="padding: 5px 0 0 0;">
                        <!-- Department : <?=$department;?> -->
                        Test
                      </td>
                    </tr>

                    <tr>
                      <td class="bodycopy">
                        <!-- Date Request : <?=$date_request;?> -->
                        Test
                      </td>
                    </tr>
                    <tr>
                      <td class="bodycopy">
                        <!-- From Leave : <?=$from_leave;?> -->
                        Test
                      </td>
                    </tr>
                    <tr>
                      <td class="bodycopy">
                        <!-- To Leave : <?=$to_leave;?> -->
                        Test
                      </td>
                    </tr>
                    <tr>
                      <td class="bodycopy">
                        <!-- Reason : <?=$reason;?> -->
                        Test
                      </td>
                    </tr>
                    <tr>
                      <td class="bodycopy">
                        <!-- Total Leave Balance : <?=$total_leave;?> /Day -->
                        Test
                      </td>
                    </tr>
                    <tr>
                      <td class="bodycopy">
                      </td>
                    </tr>
                    <tr>
                      <td class="bodycopy" style="color: #e68a00;">
                        Please check the HRMS in the system !
                      </td>
                    </tr>
                    <tr>
                      <td style="padding: 20px 0 0 0;">
                        <i style="color: #1ac6ff;">Click here to go to hrms system</i>
                        <table class="buttonwrapper" bgcolor="#e05443" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td class="button" height="45">
                              <a href="http://hrms.asiagalaxy.com/"><b style="color:white;">Click</b></a>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>

                  </td>
                </tr>
                <tr>
                  <td class="footer" bgcolor="#004d38">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="center" class="footercopy">
                          &reg; <?=date("Y")?> PT. Galaxy Investasi Harapan<br/>
                        </td>
                      </tr>
                      <tr>
                        <td align="center" style="padding: 20px 0 0 0;">
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </body>
      </html>