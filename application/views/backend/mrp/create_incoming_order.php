<!-- Page -->
<div class="page">
  <ol class="breadcrumb">
    <a href="<?php echo base_url('backend/employee/list_employee'); ?>" type="button" class="btn btn-round btn-warning"><i class="icon md-format-indent-increase" aria-hidden="true"></i>MRP List</a>
  </ol>
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="<?=base_url('backend/mrp/list_data_mrp')?>">MRP List</a></li>
    <li class="breadcrumb-item active">Create Incoming Order</li>
  </ol>
  <h4 style="text-align: left; color:#0000e6; font-weight: 900;"><b>&emsp; >>Create<< </b></h4>
  <div class="page-header" style="text-align: center; padding: 0px;">
    <h1 class="page-title">Incoming Order</h1>
  </div>
  <div class="page-content">
    <div class="panel">
      <div class="panel-body container-fluid" style="padding: 0px;">
        <div class="panel">
          <div class="panel-body container-fluid">
            <div class="row">
              <div class="col-lg-10 form-group form-material">
                <div class="pearls row">
<!--             <div class="pearl current col-3">
                    <div class="pearl-icon" style="color: green; border-color: green;"><i class="zmdi zmdi-shopping-cart-plus" aria-hidden="true"></i></div>
                    <span class="pearl-title"> <button type="button" class="btn btn-info btn-sm">&nbsp; Incoming Order &nbsp;</button>
                      <div class="pearls row d-flex justify-content-center bd-highlight mb-3">
                      </div>
                    </span>
                  </div> -->

                  <div class="pearl current col-3">
                      <div class="pearl-icon" style="color: green; border-color: green;"><i class="zmdi zmdi-shopping-cart-plus" aria-hidden="true"></i></div>
                      <span class="pearl-title"><a href="<?php echo base_url('backend/mrp/create') ?>" class="btn btn-dark btn-sm">Incoming Order</a></span>
                  </div>

                  <div class="pearl col-3">
                      <div class="pearl-icon"><i class="zmdi zmdi-settings" aria-hidden="true"></i></div>
                      <span class="pearl-title"><a href="<?php echo base_url('backend/mrp/create_part_details') ?>" class="btn btn-dark btn-sm">Part Details</a></span>
                  </div>

                  <div class="pearl col-3">
                      <div class="pearl-icon"><i class="zmdi zmdi-store" aria-hidden="true"></i></div>
                      <span class="pearl-title"><a href="<?php echo base_url('backend/mrp/create_materials') ?>" class="btn btn-dark btn-sm">Materials</a></span>
                  </div>

                  <div class="pearl col-3">
                      <div class="pearl-icon"><i class="zmdi zmdi-receipt" aria-hidden="true"></i></div>
                      <span class="pearl-title"><a href="<?php echo base_url('backend/mrp/create_production_plan') ?>" class="btn btn-dark btn-sm">Production Plan</a></span>
                  </div>


                </div>
              </div>

              <div class="col-lg-2 form-group form-material">
                <div class="pearls row">
                  <div class="col-lg-3"><br><br>
                    <button type="Submit" class="btn btn-success btn-sm" form="login_validation">&emsp; SAVE &emsp;</button>
                  </div>
                </div>
              </div>

              <div class="col-md-6 col-lg-6">
                <div class="card border">
                 <div class="card-body">
                  <div class="example-wrap">
                    <div class="example">
                      <?= form_open(base_url('backend/employee/save_emp'),  'id="login_validation" enctype="multipart/form-data"') ?>
                      <div class="form-group row form-material row">
                        <label class="col-md-3 form-control-label"><b>Cust PO No.<b style="color: red;">*</b> : </b></label>
                        <div class="col-md-9">
                          <input type="text" required="required" class="form-control" name="cust" placeholder="xx - xx"  autocomplete="off"/>
                        </div>
                      </div>

                      <div class="form-group row form-material row">
                        <label class="col-md-3 form-control-label"><b>Po Qty<b style="color: red;">*</b> : </b></label>
                        <div class="col-md-9">
                          <input type="text" required="required" class="form-control" name="first_name" placeholder="Po Qty" autocomplete="off"/>
                        </div>
                      </div>
                      <div class="form-group row form-material row">
                        <label class="col-md-3 form-control-label"><b>Opening Balance Qty<b style="color: red;">*</b> : </b></label>
                        <div class="col-md-9">
                          <input type="text" required="required" class="form-control" name="first_name"  placeholder="Opening Balance Qty" autocomplete="off"/>
                        </div>
                      </div>

                      <div class="form-group row form-material row">
                        <label class="col-md-3 form-control-label"><b>Production Plan Total<b style="color: red;">*</b>  </b></label>
                        <div class="col-md-9">
                          <input type="text" required="required" class="form-control" name="first_name"  placeholder="Production Plan Total" autocomplete="off"/>
                        </div>
                      </div>


                    </div>
                  </div>
                </div>
              </div>  
              <!-- End Example Horizontal Form -->
            </div>


            <div class="col-md-12 col-lg-6">
              <!-- Example Horizontal Form -->
              <div class="card border">
               <div class="card-body">
                <div class="example-wrap">
                  <div class="example">
                    <!-- <form class="form-horizontal"> -->
                      <div class="form-group row form-material row">
                        <label class="col-md-3 form-control-label"><b>Delivery Date <b style="color: red;">*</b>:</b></label>
                        <div class="col-md-9">
                          <input type="date" class="form-control" placeholder="Delivery Date" name="date_join_contract" required autocomplete="off"/>
                        </div>
                      </div>

                      <div class="form-group row form-material row">
                        <label class="col-md-3 form-control-label"><b>Planned Start date <b style="color: red;">*</b>:</b></label>
                        <div class="col-md-9">
                          <input type="date" class="form-control" placeholder="Planned Start date" name="date_end_contract" autocomplete="off"/>
                        </div>
                      </div>

                      <div class="form-group row form-material row">
                        <label class="col-md-3 form-control-label">Planned Completion Date<b style="color: red;">*</b> : </label>
                        <div class="col-md-9">
                          <input type="date" class="form-control" placeholder="Planned Start date" name="date_end_contract" autocomplete="off"/>
                        </div>
                      </div>

                    </div>
                  </div>
                  <!-- End Example Horizontal Form -->
                </div>
              </div>
            </div>
            <div class="col-lg-5 form-group form-material">
            </div>
            <div class="col-lg-2 form-group form-material">
            </div>
            <?php form_close() ?>
            <!-- Button Action -->
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- End Page -->
<script>
  function hanyaAngka(event) {
    var angka = (event.which) ? event.which : event.keyCode
    if (angka != 46 && angka > 31 && (angka < 48 || angka > 57))
      return false;
    return true;
  }
  var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

  function validatePassword(){
    if(password.value != confirm_password.value) {
      confirm_password.setCustomValidity("Passwords Don't Match");
    } else {
      confirm_password.setCustomValidity('');
    }
  }
  password.onchange = validatePassword;
  confirm_password.onkeyup = validatePassword;
</script>