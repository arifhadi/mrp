<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Daily Production</title>
    <table width="100%">
        <tr>
          <td width="200" align="center">
             <img src="<?php echo base_url('src/assets/images/file_head_latter/'.$edit_data->file_head_latter); ?>" width="200%">
          </td>
        </tr>
        </table> 
        <hr>
    <h2 style="font-size:100%;" align="center">
      <u>PERJANJIAN KERJA WAKTU TERTENTU</u>
      <br>No:<?=$con->contract_no_1?>
    </h2> 
</div>
</head>
    <body>
      <div class="document active">
  <div class="spreadSheetGroup">

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:50%"> 
            Pada hari ini,<b><?=$date_now_before_end?></b><br/>
            Yang bertanda tangan dibawah ini :
          </td>
          <td style="width:50%">
           <br>
          </td>
        </tr>
      </tbody>
    </table>


    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:3%"> 
            1<br/>
            <br/>
            <br/>
            <br/>
          </td>
          <td style="width:25%"> 
            Nama<br/>
            Jabatan<br/>
            Nama Perusahaan<br/>
            Alamat
          </td>
          <td style="width:72%">
           :<b>Bintang Priscilla Christy</b><br>
           :<b>HOD Accounting Finance & HR</b><br>
           :PT.Galaksi Investasi Harapan<br>
           :Jl. Ahmad Yani Panbil Industrial Estate Factory B3 Lot 3
            Mukakuning, Batam 29433
          </td>
        </tr>
      </tbody>
    </table>

  
    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%"> 
            Dalam hal ini bertindak untuk dan atas nama PT. Galaksi Investasi Harapan (perusahaan), selanjutnya disebut sebagai <b>PIHAK PERTAMA.</b>
          </td>
          
        </tr>
      </tbody>
    </table>

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:3%"> 
            2<br/>
            <br/>
            <br/>
            <br/>
          </td>
          <td style="width:25%"> 
            Nama<br/>
            No Karyawan<br/>
            Jenis Kelamin<br>
            Tempat dan tanggal lahir<br/>
            KTP No<br>
            Alamat KTP<br>
            Alamat Tinggal
          </td>
          <td style="width:72%">
           :<?=$con->name?><br>
           :<?=$con->employee_no?><br>
           :<?=$con->gender?><br>
           :<?=$con->place_of_birth?> , <?=$con->birthday?><br>
           :<?=$con->ktp?><br>
           :<?=$con->address?><br>
           :<?=$con->address?>
          </td>
        </tr>
      </tbody>
    </table>

      <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:50%"> 
            Selanjutnya disebut sebagai <b>PIHAK KEDUA</b>
          </td>
          <td style="width:50%">
          </td>
        </tr>
      </tbody>
    </table>


      <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%" align="justify"> 
            Berdasarkan kesepakatan antara PIHAK PERTAMA dan PIHAK KEDUA, kedua belah pihak menyetujui Perjanjian Kerja Waktu Tertentu (PKWT) berikut ini yang untuk selanjutnya disebut sebagai “kesepakatan kerja” sesuai dengan ketentuan dan syarat-syarat seperti yang tersebut di bawah ini :
          </td>
        </tr>
      </tbody>
    </table>

      <h3 style="font-size:100%;" align="center">
      PASAL I
      <br>PENERIMAAN DAN MASA KERJA
    </h3>

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%" align="justify"> 
            PIHAK PERTAMA menerima PIHAK KEDUA dengan jabatan sebagai <b><?=$con->designation?></b> sebagaimana halnya PIHAK KEDUA menyatakan kesediaannya untuk bekerja pada PIHAK PERTAMA terhitung sejak tanggal <?=$date_join_contract?> sampai dengan <?=$date_end_contract?>
          </td>
        </tr>
      </tbody>
    </table>

    <h3 style="font-size:100%;" align="center">
      PASAL II
      <br>UPAH DAN FASILITAS LAIN
    </h3>
    <h3 style="font-size:100%;" align="center">
      A.  UPAH BULANAN
    </h3>

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%" align="justify"> 
            PIHAK PERTAMA wajib membayarkan upah bulanan kepada PIHAK KEDUA dengan perincian  seperti yang terdapat dalam lampiran A yang terlampir dalam perjanjian kerja ini dan merupakan satu kesatuan dan bagian yang tidak terpisahkan dari perjanjian kerja ini.
          </td>
        </tr>
      </tbody>
    </table>

    <h3 style="font-size:100%;" align="center">
      B.  JAMINAN KESEHATAN
    </h3>

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%" align="justify"> 
            PIHAK PERTAMA mendaftarkan PIHAK KEDUA sebagai peserta Jaminan Pemeliharaan Kesehatan yang di selenggarakan oleh  negara dengan besarnya nilai pertanggungan disesuaikan dengan ketentuan yang berlaku.
            PIHAK PERTAMA akan mendaftarkan PIHAK KEDUA sesuai dengan standar yang diatur oleh Badan Penyelenggara Jaminan Sosial Ketenagakerjaaan (BPJS Ketenagakerjaan), yaitu untuk perlindungan:
          </td>
        </tr>
      </tbody>
    </table>

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%"> 
            - JaminanKecelakaanKerja (JKK), <br>
            - JaminanKecelakaanMati (JKM),<br>
            - JaminanHari Tua (JHT) selama waktu bekerja dengan PIHAK PERTAMA sesuai dengan ketentuan yang berlaku
          </td>
        </tr>
      </tbody>
    </table>

    <h3 style="font-size:100%;" align="center">
      C.  PAJAK PENGHASILAN
    </h3>

        <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%"> 
            Pajak penghasilan PIHAK KEDUA sepenuhnya menjadi tanggungan PIHAK KEDUA
          </td>
        </tr>
      </tbody>
    </table>

    <h3 style="font-size:100%;" align="center">
      PASAL III<br>
      KETENTUAN KERJA
    </h3>

    <h3 style="font-size:100%;" align="center">
         A.  WAKTU KERJA NORMAL
    </h3>

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%" align="justify"> 
            1.  Waktu kerja normal adalah 7 (tujuh) jam sehari dan 40 (empat puluh) jam seminggu dalam 6 (enam) hari kerja, atau 8 (delapan) jam sehari dan 40 (empat puluh) jam seminggu dalam 5 (lima) hari kerja.<br>
            2.  Istirahat Mingguan mengikuti jadwal yang ditentukan oleh PIHAK PERTAMA sepanjang tidak melebihi waktu kerja normal yang disebutkan pada pasal III (A) ayat 1
          </td>
        </tr>
      </tbody>
    </table>


    <h3 style="font-size:100%;" align="center">
        B.  BEKERJA LEMBUR
    </h3>

      <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%" align="justify"> 
            Bekerja lembur hanya boleh dilaksanakan atas permintaan perusahaan. Perhitungan upah lembur akan didasarkan pada peraturan mengenai pembayaran upah lembur yang berlaku.
          </td>
        </tr>
      </tbody>
    </table>

    <h3 style="font-size:100%;" align="center">
        C.  KERJA SHIFT
    </h3>

      <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%" align="justify"> 
            PIHAK KEDUA dapat ditugaskan bekerja shift dan diatur secara bergiliran. Untuk shift malam yang bekerja pada pukul 23.00 malam keatas, PIHAK KEDUA diberikan tunjangan shift malam yang jumlahnya disesuaikan dengan kebijaksanaan/kemampuan perusahaan. Tunjangan ini merupakan pengganti tambahan nutrisi bagi pekerja shift malam tersebut diatas
          </td>
        </tr>
      </tbody>
    </table>

    <h3 style="font-size:100%;" align="center">
        D.  HAK CUTI KERJA
    </h3>

      <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%" align="justify"> 
            1.  PIHAK KEDUA mendapatkan hak cuti pertahun sebanyak 12 hari, kecuali diatur lain berdasarkan Keputusan tertulis dari Management<br>
            2.  Selama 3 bulan pertama masa kerja PIHAK KEDUA, hak cuti tahunan belum timbul, dan belum bisa diambil.<br>
            3.  Hak cuti tahunan PIHAK KEDUA timbul pada akhir bulan dibulan ke-4 sebanyak 4 hari, dan penambahan 1 hari setiap bulan berikutnya dalam 1 tahun kalender masa kerja<br>
            4.  Pengambilan cuti melebihi hak cuti tahunan yang jatuh tempo, maka kelebihan cuti yang diambil tersebut akan dianggap cuti tanpa dibayar<br>
            5.  Semua hak cuti tahunan harus diambil paling lama 3 bulan setelah masa kerja 1 tahun kalender berakhir.<br>
            6.  Sisa cuti tahunan yang tidak diambil dan masih tersisa di akhir bulan ke-3 setelah masa kerja 1 tahun kalender berakhir, akan dianggap hangus dan tidak bisa diambil lagi dan / atau tidak bisa dikompensasi / diganti dengan cuti tahunan lain ataupun penggantian dengan uang.<br>
            7.  PIHAK KEDUA diperbolehkan mengambil cuti dengan seizin atasan.
          </td>
        </tr>
      </tbody>
    </table>

    <h3 style="font-size:100%;" align="center">
        E.  TUNJANGAN HARI RAYA 
    </h3>

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%" align="justify"> 
            1.  Perusahaan memisahkan pembayaran THR bagi karyawan Muslim dan non Muslim<br>
            2.  Tunjangan Hari Raya (THR) akan dibayarkan selambat-lambatnya 7 (tujuh) hari sebelum Hari Raya Keagamaan, baik bagi karyawan Muslim dan non Muslim.<br>
            3.  PIHAK KEDUA tidak berhak atas Tunjangan Hari Raya (THR) jika masa kerjanya berakhir sebelum hari raya keagamaan<br>
            4.  Tunjangan Hari Raya (THR) diberikan dengan ketentuan sebagai berikut:
          </td>
        </tr>
      </tbody>
    </table>

      <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:2%"> 
          </td>
          <td style="width:98%" align="justify"> 
            a.  PIHAK KEDUA telah mempunyai masa kerja satu bulan terus menerus atau lebih dan masa kerja belum berakhir sebagaimana tertulis pada pasal III bagian E ayat 3.<br>
            b.  Dalam hal PIHAK KEDUA yang telah mempunyai masa kerja 12 (dua belas) bulan secara terus menerus atau lebih, diberikan sebesar 1 (satu) bulan upah<br>
            c.  Dalam hal PIHAK KEDUA yang mempunyai masa kerja 1 (satu) bulan secara terus menerus tetapi kurang dari 12 (dua belas) bulan, diberikan secara proporsional sesuai masa kerja dengan perhitungan: masa kerja (dalam bulan) x 1 (satu) bulan upah dibagi 12.
          </td>
        </tr>
      </tbody>
    </table>


    <h3 style="font-size:100%;" align="center">
        F. BEKERJA UNTUK PIHAK KETIGA 
    </h3>

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%" align="justify"> 
            Selama berlangsungnya perjanjian kerja PIHAK KEDUA tidak diperkenankan melakukan pekerjaan-pekerjaan atau tugas-tugas untuk pihak lain, kecuali dengan persetujuan tertulis dari PIHAK PERTAMA dan PIHAK KEDUA menyetujui persetujuan tersebut.
          </td>
        </tr>
      </tbody>
    </table>

    <h3 style="font-size:100%;" align="center">
        G.  MANGKIR 
    </h3>

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%" align="justify"> 
            1.  PIHAK KEDUA dianggap mangkir apabila tidak masuk kerja selama 5 (lima) hari kerja atau lebih berturut-turut tanpa adanya keterangan secara tertulis yang dilengkapi dengan bukti yang sah dan telah dipanggil oleh PIHAK PERTAMA sebanyak 2 (dua) kali secara patut dan tertulis<br>
            2.  Alamat korespondensi pemanggilan kepada PIHAK KEDUA berdasarkan ayat 1 (satu) di atas adalah alamat yang dinyatakan oleh PIHAK KEDUA sesuai Kartu Tanda Penduduk (KTP) dari PIHAK KEDUA pada saat masuk bekerja<br>
            3.  Setelah dilakukan pemanggilan secara patut dan tertulis oleh PIHAK PERTAMA terhadap PIHAK KEDUA, apabila PIHAK KEDUA masih mangkir setelah surat panggilan kedua, maka PIHAK KEDUA dianggap mengundurkan diri secara sukarela atau atas dasar kemauan PIHAK KEDUA sendiri untuk mengakhiri hubungan kerja dengan PIHAK PERTAMA.<br>
            4.  Sebagaimana dimaksud pada ayat 3 (tiga), PIHAK PERTAMA berhak memproses pemutusan hubungan kerja dan Kedua Belah Pihak sepakat untuk berpedoman dan mengacu kepada Pasal 62 UU no. 13 tahun 2003 dan Pasal 17 PP Nomor 35 Tahun 2021 untuk perhitungan ganti rugi atau kompensasi.<br>
            5.  PIHAK PERTAMA berhak memotong gaji pokok dan tunjangan-tunjangan lain bila PIHAK KEDUA tidak masuk kerja tanpa alasan-alasan yang dapat diterima.
          </td>
        </tr>
      </tbody>
    </table>


    <h3 style="font-size:100%;" align="center">
        H.  ROTASI 
    </h3>

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%" align="justify"> 
            1.  Sesuai kebutuhan, PIHAK KEDUA dapat dipindahkan setiap waktu oleh Perusahaan dari departemennya, kantor, pabrik, atau tempat lain dari pekerjaan di Indonesia atau cabang diluar negeri, ke departemen lainnya dalam satu perusahaan, perusahaan afiliasi atau ke anak perusahaan lainnya untuk melakukan beberapa tugas serta pekerjaan<br>
            2.  Hal mengenai pemindahan tersebut akan diatur Surat Keputusan atau surat tugas oleh management perusahaan tanpa perlu ada persetujuan dari PIHAK KEDUA sepanjang perpindahan tersebut tidak menimbulkan perubahan terhadap gaji pokok dan tunjangan tetap PIHAK KEDUA.
          </td>
        </tr>
      </tbody>
    </table>


    <h3 style="font-size:100%;" align="center">
        PASAL IV<br>
        PEMUTUSAN HUBUNGAN KERJA
    </h3>

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%" align="justify"> 
            1.  PIHAK PERTAMA berhak untuk melakukan pemutusan hubungan kerja dengan PIHAK KEDUA tanpa diwajibkan untuk membayar pesangon atau kompensasi apapun apabila PIHAK KEDUA tidak masuk kerja selama 5 (lima) hari berturut-turut tanpa keterangan secara tertulis, dan telah dipanggil oleh PIHAK PERTAMA 2 (dua) kali secara patut dan tertulis. Dalam hal ini PIHAK KEDUA dianggap mengundurkan diri secara sukarela atau atas dasar kemauan PIHAK KEDUA sendiri untuk mengakhiri hubungan kerja dengan PIHAK PERTAMA dan PIHAK KEDUA tidak berhak menuntut kompensasi apapun dari PIHAK PERTAMA<br>
            2.  Apabila salah satu pihak mengakhiri hubungan kerja sebelum berakhirnya jangka waktu yang ditetapkan dalam perjanjian kerja waktu tertentu (PKWT), meskipun Pihak Kedua mengajukan surat pengunduran diri baik disetujui atau tidak oleh PIHAK PERTAMA maka Kedua Belah Pihak sepakat untuk berpedoman dan mengacu kepada Pasal 62 UU no. 13 tahun 2003 dan Pasal 17 PP Nomor 35 Tahun 2021 untuk perhitungan ganti rugi atau kompensasi<br>
            3.  Apabila PIHAK KEDUA melakukan pelanggaran berat menyangkut hal-hal yang tersebut dibawah ini, maka PIHAK PERTAMA dibenarkan untuk memutuskan hubungan kerja. Pelanggaran berat yang dimaksud adalah:
          </td>
        </tr>
      </tbody>
    </table>

        <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:3%"> 
           3.1<br>
           3.2<br>
           3.3<br>
           <br>
           3.4<br>
           3.5<br>
           3.6<br>
           3.7<br>
           3.8<br>
           3.9<br>
           <br> 
           3.10<br>
           <br>
           3.11<br>
           <br>
           3.12<br>
          </td>
          <td style="width:97%" align="justify"> 
           Melakukan penipuan, pencurian, atau penggelapan barang dan/atau uang milik perusahaan<br>
           Memberikan keterangan palsu atau yang dipalsukan sehingga merugikan perusahaan.<br>
           Mabuk, meminum minuman keras yang memabukkan, memakai dan atau mengedarkan narkotika, psikotropika, dan zat adiktif lainnya di  dalam dan diluar lingkungan  kerja.<br>
           Melakukan perbuatan asusila atau perjudian di  dalam dan diluar  lingkungan kerja.<br>
           Menyerang, menganiaya, mengancam, atau mengintimidasi teman sekerja, atasan atau tamu perusahaan di lingkungan kerja<br>
           Membujuk teman sekerja atau pengusaha untuk melakukan perbuatan bertentangan dengan peraturan perundang-undangan<br>
           Dengan ceroboh atau sengaja merusak atau membiarkan teman sekerja atau pengusaha dalam keadaan bahaya di tempat kerja.<br>
           Membongkar atau membocorkan rahasia perusahaan yang seharusnya dirahasiakan kecuali untuk kepentingan Negara.<br>
           Melakukan perbuatan-perbuatan/tindakan-tindakan yang dapat menimbulkan kerugian terhadap PIHAK  PERTAMA, maupun sesama teman sekerja baik secara sengaja maupun disebabkan oleh kecerobohannya yang dilakungan dalam lingkungan perusahaan<br>
           Melakukan perbuatan-perbuatan/tindakan-tindakan yang dapat dianggap mengganggu ketenangan, ketertiban dan keamanan kerja diantara sesama rekan kerja di lingkungan perusahaan.<br>
           Melakukan perbuatan melanggar hukum di dalam maupun di luar perusahaan dengan ancaman pidana, baik sebelum ataupun setelah diputuskan pengadilan.<br>
           Masuk penjara akibat terbukti melakukan perbuatan melanggar hukum yang dilakukan di dalam ataupun di luar perusahaan.
          </td>
        </tr>
      </tbody>
    </table>

      <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%" align="justify"> 
            4.  PIHAK KEDUA mentaati dengan sepenuhnya seluruh ketentuan dalam perjanjian kerja ini, berikut semua peraturan perusahaan PIHAK PERTAMA maupun perusahaan dimana PIHAK KEDUA ditugaskan. <br>
            5.  Ketidaktaatan terhadap ketentuan-ketentuan tersebut mengakibatkan pemutusan hubungan kerja yang dilaksanakan sesuai dengan peraturan perundang-undangan yang berlaku.
          </td>
        </tr>
      </tbody>
    </table>


    <h3 style="font-size:100%;" align="center">
        PASAL VI<br>
        LAIN – LAIN
    </h3>

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%" align="justify"> 
            1.  Jika ada dalam ketentuan kerja ini yang dilarang, cacat dan dapat dimintakan pembatalan atau bertentangan dengan hukum atau peraturan dari pejabat berwenang, maka hanya ketentuan itu saja yang tidak berlaku sedangkan ketentuan-ketentuan lainnya tetap berlaku. Para pihak dengan itikad baik akan mencari ketentuan penggantinya.<br>
            2.  Didalam hal ada ketentuan yang belum tercantum dalam perjanjian kerja ini, belum diatur maupun akan dikeluarkan oleh PIHAK PERTAMA, apabila dianggap perlu, maka tambahan dan akan diberlakukan dipatuhi kedua belah pihak. Tambahan atas perjanjian tersebut mempunyai kekuatan hukum yang sama dan mengikat didalam Perjanjian Kerja ini<br>
            3.  Hal-hal yang belum diatur dalam perjanjian kerja ini akan mengikuti ketentuan-ketentuan/peraturan-peraturan ketenagakerjaan yang berlaku.<br>
            4.  Perjanjian kerja ini mengikat kedua belah pihak dan merupakan pengganti dari semua persetujuan sebelumnya baik secara lisan maupun tertulis dan perjanjian ini tidak dapat diganti dengan alasan apapun tanpa persetujuan tertulis dari kedua belah pihak
          </td>
        </tr>
      </tbody>
    </table>

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%" align="justify"> 
            Perjanjian kerja ini dibuat dan ditandatangani oleh kedua belah pihak secara sadar dan tanpa adanya tekanan dari pihak manapun dan berlaku sejak tanggal ditandatanganinya
          </td>
        </tr>
      </tbody>
    </table>

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:4%"> 
            Batam, <?=$date_now_before_end?>
          </td>
        </tr>
      </tbody>
    </table>

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:30%"> 
            <b>PIHAK PERTAMA</b>
            <br>
            <br>
            <br>
            <br>
            <br>
            <u>Bintang Priscilla Christy</u><br>
            <b>HOD Accounting Finance & HR</b>
          </td>
          <td style="width:40%"> 
            <b>MENGETAHUI</b>
            <br>
            <br>
            <br>
            <br>
            <br>
            <u>Phuspitaa Naaidu Ramloo</u><br>
            <b>President Director PT. Galaksi Investasi Harapan</b>
          </td>
          <td style="width:30%"> 
            <b>PIHAK KEDUA</b>
            <br>
            <br>
            <br>
            <br>
            <br>
            <u><?=$con->name?></u><br>
            <b>Karyawan</b> 
          </td>
        </tr>
      </tbody>
    </table>

    <h4 style="font-size:100%;" align="right">
       <i>Lampiran A</i>
    </h4>

     <h3 style="font-size:100%;" align="center">
       UPAH PER-BULAN
    </h3>

      <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:100%"> 
            PIHAK KEDUA akan menerima gaji dengan rincian sebagai berikut :
          </td>
        </tr>
      </tbody>
    </table>

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:30%"> 
            Gaji Pokok<br>
            Tunjangan Tetap<br>
            Tunjangan Tidak Tetap
          </td>
          <td style="width:70%"> 
            :<?=$con->basic_salary;?><br>
            :<?=$con->incentive;?><br>
            :<?=$con->fix_allowance;?>
          </td>
        </tr>
      </tbody>
    </table>

    <p style="font-size:90%;"><u>Catatan :</u>
    <p style="font-size:70%;">• Tunjangan tidak tetap dihitung berdasarkan jumlah kehadiran</p>
    <p style="font-size:70%;">• Tunjangan tidak tetap tidak dihitung jika ada UPL dan Absent </p>
    <p style="font-size:70%;">• Perhitungan upah lembur dalam satu jam adalah 1/173 x (Gaji Pokok + Tunjangan Tetap
    <p style="font-size:70%;">• Pemberian upah kerja lembur hanya untuk  jam kerja lembur berdasarkan form kerja lembur yang telah disetujui oleh perusahaan.
    <p style="font-size:70%;">• Perubahan upah akan disesuaikan dengan ketentuan pemerintah dan atau kebijaksanaan pengupahan yang berlaku di perusahaan

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:4%"> 
            Batam, <?=$date_now_before_end?>
          </td>
        </tr>
      </tbody>
    </table>

    <table class="shipToFrom">
      <thead style="font-weight:bold">
        <tr>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:30%"> 
            <b>PIHAK PERTAMA</b>
            <br>
            <br>
            <br>
            <br>
            <br>
            <u>Bintang Priscilla Christy</u><br>
            <b>HOD Accounting Finance & HR</b>
          </td>
          <td style="width:40%"> 
            <b>MENGETAHUI</b>
            <br>
            <br>
            <br>
            <br>
            <br>
            <u>Phuspitaa Naaidu Ramloo</u><br>
            <b>President Director PT. Galaksi Investasi Harapan</b>
          </td>
          <td style="width:30%"> 
            <b>PIHAK KEDUA</b>
            <br>
            <br>
            <br>
            <br>
            <br>
            <u><?=$con->name?></u><br>
            <b>Karyawan</b> 
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
    </body>
<style type="text/css">
  /* Housekeeping */
body{
  font-size:12px;
}
.spreadSheetGroup{
    /*font:0.75em/1.5 sans-serif;
    font-size:14px;
  */
    color:#333;
    background-color:#fff;
    padding:1em;
}

/* Tables */
.spreadSheetGroup table{
    width:100%;
    margin-bottom:1em;
    border-collapse: collapse;
}
.spreadSheetGroup .proposedWork th{
    background-color:#eee;
}
.tableBorder th{
  background-color:#eee;
}
.spreadSheetGroup th,
.spreadSheetGroup tbody td{
    padding:0.5em;

}
.spreadSheetGroup tfoot td{
    padding:0.5em;

}
.spreadSheetGroup td:focus { 
  border:1px solid #fff;
  -webkit-box-shadow:inset 0px 0px 0px 2px #5292F7;
  -moz-box-shadow:inset 0px 0px 0px 2px #5292F7;
  box-shadow:inset 0px 0px 0px 2px #5292F7;
  outline: none;
}
.spreadSheetGroup .spreadSheetTitle{ 
  font-weight: bold;
}
.spreadSheetGroup tr td{
  text-align:center;
}
/*
.spreadSheetGroup tr td:nth-child(2){
  text-align:left;
  width:100%;
}
*/

/*
.documentArea.active tr td.calculation{
  background-color:#fafafa;
  text-align:right;
  cursor: not-allowed;
}
*/
.spreadSheetGroup .calculation::before, .spreadSheetGroup .groupTotal::before{
  /*content: "$";*/
}
.spreadSheetGroup .trAdd{
  background-color: #007bff !important;
  color:#fff;
  font-weight:800;
  cursor: pointer;
}
.spreadSheetGroup .tdDelete{
  background-color: #eee;
  color:#888;
  font-weight:800;
  cursor: pointer;
}
.spreadSheetGroup .tdDelete:hover{
  background-color: #df5640;
  color:#fff;
  border-color: #ce3118;
}
.documentControls{
  text-align:right;
}
.spreadSheetTitle span{
  padding-right:10px;
}

.spreadSheetTitle a{
  font-weight: normal;
  padding: 0 12px;
}
.spreadSheetTitle a:hover, .spreadSheetTitle a:focus, .spreadSheetTitle a:active{
  text-decoration:none;
}
.spreadSheetGroup .groupTotal{
  text-align:right;
}



table.style1 tr td:first-child{
  font-weight:bold;
  white-space:nowrap;
  text-align:right;
}
table.style1 tr td:last-child{
  border-bottom:1px solid #000;
}



table.proposedWork td,
table.proposedWork th,
table.exclusions td,
table.exclusions th{
  border:1px solid #000;
}
table.proposedWork thead th, table.exclusions thead th{
  font-weight:bold;
}
table.proposedWork td,
table.proposedWork th:first-child,
table.exclusions th, table.exclusions td{
  text-align:left;
  vertical-align:top;
}
table.proposedWork td.description{
  width:20%;
}

table.proposedWork td.amountColumn, table.proposedWork th.amountColumn,
table.proposedWork td:last-child, table.proposedWork th:last-child{
  text-align:center;
  vertical-align:top;
  white-space:nowrap;
}

.amount:before, .total:before{
  content: "$";
}
table.proposedWork tfoot td:first-child{
  border:none;
  text-align:right;
}
table.proposedWork tfoot tr:last-child td{
  font-size:16px;
  font-weight:bold;
}

table.style1 tr td:last-child{
  width:100%;
}
table.style1 td:last-child{
  text-align:left;
}
td.tdDelete{
  width:1%;
}

table.coResponse td{text-align:left}
table.shipToFrom td, table.shipToFrom th{text-align:left}

.docEdit{border:0 !important}

.tableBorder td, .tableBorder th{
  border:1px solid #000;
}
.tableBorder th, .tableBorder td{text-align:center}

table.proposedWork td, table.proposedWork th{text-align:center}
table.proposedWork td.description{text-align:left}
</style>

</html>