<!-- <main class="text-center" id="main"> -->
<div class="page">
  <div class="page-header" style="padding: 20px 10px;">
    <ol class="breadcrumb">
      <a href="<?php echo base_url('backend/mrp/create'); ?>" class="btn btn-round btn-danger"><i class="icon md-plus" aria-hidden="true"></i>&nbsp; Create &nbsp;</a>&emsp;&emsp;
      <button type="button" class="btn btn-success btn-round" data-target="#import-designation" data-toggle="modal"><i class="icon md-upload" aria-hidden="true"></i>Import Excel</button>&emsp;&emsp;
    </ol><br>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?=base_url('backend/admdashboard')?>">Dashboard</a></li>
      <!-- <li class="breadcrumb-item"><a href="<?=base_url('backend/employee')?>">Master Data Employee</a></li> -->
      <li class="breadcrumb-item active">MRP List</li>
    </ol>
  </div>
  <?php if ($this->session->flashdata('success')) { ?>
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button><p><?php echo $this->session->flashdata('success'); ?></p>
    </div>
  <?php }elseif($this->session->flashdata('error')){ ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button><p><?php echo $this->session->flashdata('error'); ?></p>
    </div>
  <?php } ?>
  <h3 class="panel-title" style="text-align: center; padding: 0px;"><b>MRP List</b></h3>
  <div class="page-content" style="padding: 0px 0px;">
    <div class="panel"><br>
      <div class="panel-body">
      <input type="checkbox" id="toggle-right">
        <div class="page-wrap">
          <div class="top-bar-right">
            <label for="toggle-right" class="profile-toggle">Detail Data</label>
          </div>
          <div class="profile">
            <!-- <p id="testValue">sadsa asfsaf</p> -->
            <h4><strong>Detail Data</strong></h4>
            <h5>Gih Tool No</h5>
            <p id="gihToolNo"></p>
            <h5>Tooling Name</h5>
            <p id="toolingName"></p>
            <h5>Tooling No</h5>
            <p id="toolingNo"></p>
            <h5>Drawing No</h5>
            <p id="drawingNo"></p>
            <h5>Ownership</h5>
            <p id="ownerShip"></p>
            <h5>Sampling</h5>
            <p id="sampling"></p>
            <h5>Fail</h5>
            <p id="fail"></p>
            <h5>Passed</h5>
            <p id="passed"></p>
            <h5>Project Name</h5>
            <p id="project"></p>
          </div>
        </div>
      <input type="checkbox" id="toggle-left">
        <div class="page-wrap">
          <div class="top-bar-left">
            <label for="toggle-left" class="prof-toggle">Filter Data</label>
          </div>
          <div class="prof">
            <h4><strong>Filter Data</strong></h4>
            <div class="form-group row form-material row">
              <div class="col-md-10">
                <input type="text" class="form-control" id="gih_tool_nos" name="gih_tool_nos" placeholder="Gih Tool No" autocomplete="off"/>
              </div>
            </div>
            <div id="toolingNames" style="display: none;">
            <div class="form-group row form-material row">
              <div class="col-md-10">
                <input type="text" class="form-control" id="tooling_names" name="tooling_names" placeholder="Tooling Name" autocomplete="off"/>
              </div>
            </div>
            </div>
            <div id="toolingsNo" style="display: none;">
            <div class="form-group row form-material row">
              <div class="col-md-10">
                <input type="text" class="form-control" id="tooling_nos" name="tooling_nos" placeholder="Tooling No" autocomplete="off"/>
              </div>
            </div>
            </div>
            <h5></h5>
            <div id="drawingsNo" style="display: none;">
            <div class="form-group row form-material row">
              <div class="col-md-10">
                <input type="text" class="form-control" id="drawing_nos" name="drawing_nos" placeholder="Drawing No" autocomplete="off"/>
              </div>
            </div>
            </div>
            <h5></h5>
           <div id="ownerships" style="display: none;">
            <div class="form-group row form-material row">
              <div class="col-md-10">
                <input type="text" class="form-control" id="ownership" name="ownership" placeholder="Ownership" autocomplete="off"/>
              </div>
            </div>
            </div>
            <div id="samplings" style="display: none;">
            <div class="form-group row form-material row">
              <div class="col-md-10">
                <input type="text" class="form-control" id="sampling" name="sampling" placeholder="Sampling" autocomplete="off"/>
              </div>
            </div>
            </div>
           <div id="fails" style="display: none;">
            <div class="form-group row form-material row">
              <div class="col-md-10">
                <input type="text" class="form-control" id="fail" name="fail" placeholder="Fail" autocomplete="off"/>
              </div>
            </div>
            </div>
           <div id="passeds" style="display: none;">
            <div class="form-group row form-material row">
              <div class="col-md-10">
                <input type="text" class="form-control" id="passed" name="passed" placeholder="Passed" autocomplete="off"/>
              </div>
            </div>
            </div>
            <div id="projectNames" style="display: none;">
            <div class="form-group row form-material row">
              <div class="col-md-10">
                <input type="text" class="form-control" id="project_name" name="project_name" placeholder="Project Name" autocomplete="off"/>
              </div>
            </div>
            </div>
            <div id="btnAddColumn" style="display: block;">
            <br>
            <button type="button" onclick="addColumn()" class="btn btn-success btn-round"><i class="zmdi zmdi-plus" aria-hidden="true"></i>&emsp;Add Column&emsp;</button>
            </div>
            <br>
            <button type="button" onclick="filterColumn()"class="btn btn-success btn-round"><i class="icon zmdi zmdi-search" aria-hidden="true"></i>&emsp;&emsp;Search&emsp;&emsp;</button>
          </div>
        </div>

        <table id='empTable' class='table table-hover dataTable table-striped w-full'>
         <thead>
           <tr>
             <th>Gih Tool No</th>
             <th>Tooling Name</th>
             <th>Tooling No</th>
             <th>Drawing No</th>
             <th>Ownership</th>
             <th>Sampling</th>
             <th>Fail</th>
             <th>Passed</th>
             <th>Project Name</th>
             <th>Part No</th>
           </tr>
         </thead>
       </table>
     </div>
   </div>
 </div>

</div>
<!-- </main> -->
<!-- Modal -->

<style type="text/css">
html, body {
     height: 100%;
}
 .spinner {
     z-index: 2;
     position: fixed;
     top: 50%;
     left: 50%;
     margin: 0 auto;
     width: 32px;
     height: 32px;
}
 .spinner .path {
     stroke: #0097e6;
     stroke-linecap: round;
     -webkit-animation: dash 1.1s ease-in-out infinite;
     animation: dash 1.1s ease-in-out infinite;
}
 @keyframes dash {
     0% {
         stroke-dasharray: 1, 160;
         stroke-dashoffset: 0;
    }
     50% {
         stroke-dasharray: 80, 160;
         stroke-dashoffset: -32;
    }
     100% {
         stroke-dasharray: 80, 160;
         stroke-dashoffset: -124;
    }
}
div.dataTables_wrapper div.dataTables_processing {
    position: relative;
    top: 50%;
    left: 50%;
    width: 200px;
    padding: 1em 0;
    margin-top: -26px;
    margin-left: -100px;
    text-align: center;
    background: transparent;
}
.table {
    color: #757575;
    cursor: pointer;
}


/*start sidebar*/
.top-bar-right {
  position: fixed;
  right: 0; /* change*/
  z-index: 2;
  top: 400px;
}
.profile-toggle {
  position: absolute;
  cursor: pointer;
  width: 200px;
  /*height: 100%;*/
  display: table;
  /*border-radius:5px;*/
  font-family:'cairo',serif;
  font-size: 16px;
  right: -90px; /*change*/
  -webkit-transition: all 0.15s ease-out 0s;
  -moz-transition: all 0.15s ease-out 0s;
  transition: all 0.15s ease-out 0s;
  z-index: 2;
  -webkit-transform: rotate(-90deg);
  transform: rotate(-90deg);
  background: #e67e22;
  color: #ffffff;
  text-align: center;
  padding: 5px;
}
#toggle-right {
  display: none;
}
#toggle-right:checked + .page-wrap .profile {
  right: 0px;  /*change*/
}
#toggle-right:checked + .page-wrap .profile-toggle {
  right: 166px; /*change*/
}
.profile {
  position: fixed;
  top: 70px;
  right: -270px; /*change*/
  -webkit-transition: all 0.15s ease-out 0s;
  -moz-transition: all 0.15s ease-out 0s;
  transition: all 0.15s ease-out 0s;
  height: 100%;
  width: 250px;
  border-radius:5px;
  background: #ffffff;
  z-index: 2000;
  box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
  /*border: 1px solid #C8102E;*/
  padding: 10px 20px;
  font-family:'sans-serif',serif;
  /*color: #000000;*/
  text-align: left;
}
.profile a {
  display: block;
  margin-bottom: 0px;
}
/*End Sidebar right*/

/*Start Sidebar Left*/

.top-bar-left {
  position: fixed;
  left: 0; /* change*/
  z-index: 2;
  top: 400px;
}
.prof-toggle {
  position: absolute;
  cursor: pointer;
  width: 200px;
  /*height: 100%;*/
  display: table;
  /*border-radius:5px;*/
  font-family:'cairo',serif;
  font-size: 16px;
  left: -80px; /*change*/
  -webkit-transition: all 0.15s ease-out 0s;
  -moz-transition: all 0.15s ease-out 0s;
  transition: all 0.15s ease-out 0s;
  z-index: 2;
  -webkit-transform: rotate(-90deg);
  transform: rotate(-90deg);
  background: #e1b12c;
  color: #ffffff;
  text-align: center;
  padding: 3px;
}
#toggle-left {
  display: none;
}
#toggle-left:checked + .page-wrap .prof {
  left: 0px;  /*change*/
}
#toggle-left:checked + .page-wrap .prof-toggle {
  left: 166px; /*change*/
}
.prof {
  position: fixed;
  top: 70px;
  left: -270px; /*change*/
  -webkit-transition: all 0.15s ease-out 0s;
  -moz-transition: all 0.15s ease-out 0s;
  transition: all 0.15s ease-out 0s;
  height: 100%;
  width: 250px;
  border-radius:5px;
  background: #ffffff;
  z-index: 2000;
  box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
  /*border: 1px solid #C8102E;*/
  padding: 10px 20px;
  font-family:'sans-serif',serif;
  /*color: #000000;*/
  text-align: left;
}
.prof a {
  display: block;
  margin-bottom: 0px;
}
/*End Sidebar Left*/



</style>

<!-- Modal -->
<div class="modal fade" id="import-designation" aria-hidden="true" aria-labelledby="filterdata" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple modal-center">
    <div class="modal-content">
      <?= form_open(base_url('backend/employee/import_designation'),  'id="login_validation" enctype="multipart/form-data"') ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <h4 class="modal-title" style="text-align: center;">Upload File Here</h4>
      <div class="modal-body">
        <div class="example-grid">
          <div class="row">
            <div class="col-md-9">
              <a href="<?php echo base_url('backend/employee/template_emp/Template_Designation.xlsx') ?>">Download Template.xlsx</a>
              <div class="input-group input-group-file" data-plugin="inputGroupFile">
                <input type="text" class="form-control" readonly="">
                <span class="input-group-append">
                  <span class="btn btn-success btn-file waves-effect waves-light waves-round">
                    <i class="icon md-upload" aria-hidden="true"></i>
                    <!-- <input type="file" name="file_picture" multiple=""> -->
                    <input type="file" class="form-control" name="file" accept=".xls, .xlsx" required>
                  </span>
                </span>
              </div>
            </div>
            <div class="col-lg-6">
              <span class="text-secondary">file format : <b style="color:red;">.xls, xlsx</b></span>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="Submit" class="btn btn-success btn-sm">&emsp;&nbsp; IMPORT &emsp;&nbsp;</button>&emsp;
        <?php form_close() ?>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL -->

<script type="text/javascript">//On-Progress

var squence = 0;

function addColumn()
{

  var toolingNames = document.getElementById("toolingNames");
  var toolingsNo = document.getElementById("toolingsNo");
  var drawingsNo = document.getElementById("drawingsNo");
  var ownerships = document.getElementById("ownerships");
  var fails = document.getElementById("fails");
  var passeds = document.getElementById("passeds");
  var projectNames = document.getElementById("projectNames");
  var btnAddColumn = document.getElementById("btnAddColumn");
  squence += 1;
  if(squence==1)
  {
    toolingNames.style.display = "block";
  }
  else if(squence==2)
  {
    toolingsNo.style.display = "block";
  }
  else if(squence==3)
  {
    drawingsNo.style.display ="block";
  }
  else if(squence==4)
  {
    ownerships.style.display="block";
  }
  else if(squence==5)
  {
    samplings.style.display="block";
  }
  else if(squence==6)
  {
    fails.style.display="block";
  }
  else if(squence==7)
  {
    passeds.style.display="block";
  }
  else if(squence==8)
  {
    projectNames.style.display="block";
    btnAddColumn.style.display="none"
  }

  // console.log(squence);
}
function filterColumn()
{

    let gihTool_No = document.getElementById('gih_tool_nos').value;
    let tooling_Names = document.getElementById('tooling_names').value;
    let tooling_No = document.getElementById('tooling_nos').value;
    $.ajax({
        method: 'POST',
        url: "<?= base_url("backend/mrp/filter_data_mrp/")?>",
        data: {'gih_tool_no': gihTool_No,'tooling_name':tooling_Names,'tooling_no':tooling_No},
        cache: false,
        async : true,
        dataType : 'json',
        success: function(data){
          $('#empTable').DataTable().clear();
          $('#empTable').DataTable().destroy();
          var table = $('#empTable').DataTable({
          'processing': true,
          'responsive':true,
          'language':{
            'loadingRecords': '&nbsp;', 
            "processing": '<svg class="spinner" viewBox="0 0 16 18"><path class="path" fill="none" stroke-width="2" d="M7.21487 1.2868C7.88431 0.9044 8.73031 0.9044 9.39974 1.2868L9.40283 1.28856L14.4613 4.20761C15.1684 4.598 15.5746 5.33558 15.5746 6.11465V8.99996V11.8853C15.5746 12.6507 15.1632 13.3848 14.4617 13.7721L9.37973 16.7132C8.71029 17.0956 7.86428 17.0956 7.19485 16.7132L7.19088 16.7109L2.11279 13.772C1.40602 13.3816 1 12.6441 1 11.8653V8.98995V6.11465C1 5.31458 1.44381 4.59039 2.10827 4.21051L7.21487 1.2868Z"/></svg>'          
          },
          'data':data,
          'columns': [
             { data: 'gih_tool_no'},
             { data: 'tooling_name'},
             { data: 'tooling_no'},
             { data: 'drawing_no'},
             { data: 'ownership'},
             { data: 'sampling'},
             { data: 'fail'},
             { data: 'passed'},
             { data: 'project_name'},
             { data: 'part_no'},
          ],
          'select': true,
          'select': 'single',
        });
        }
      });
}
$(".change").click(function(){
  var id = $(this).attr("data-bind");
  swal({
    title: "you want to change the data?",
    text: "",
    type: "info",
    showCancelButton: true,
    confirmButtonClass: "btn-info",
    confirmButtonText: "Yes",
    cancelButtonText: "Cancel",
    closeOnConfirm: false,
    closeOnCancel: false
  },
  function(isConfirm) {
    if (isConfirm) {
      $.ajax({
        success: function(data) {
          window.location.href = '<?= base_url("backend/employee/update_designation/")?>'+id;
        }
      });
    } else {
      swal("Cancelled", "Your imaginary file is safe :)", "error");
    }
  });
});

$(".delete").click(function(){
  var id = $(this).attr("data-bind");
  swal({
    title: "you want to delete the data?",
    text: "",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Yes",
    cancelButtonText: "Cancel",
    closeOnConfirm: false,
    closeOnCancel: false
  },
  function(isConfirm) {
    if (isConfirm) {
      $.ajax({
        success: function(data) {
          window.location.href = '<?= base_url("backend/employee/delete_designation/")?>'+id;
        }
      });
    } else {
      swal("Cancelled", "Your imaginary file is safe :)", "error");
    }
  });
});

function displayDataTables()
{
     var table = $('#empTable').DataTable({
          'processing': true,
          'responsive':true,
          "paging": true,
          'language':{
            'loadingRecords': '&nbsp;', 
            "processing": '<svg class="spinner" viewBox="0 0 16 18"><path class="path" fill="none" stroke-width="2" d="M7.21487 1.2868C7.88431 0.9044 8.73031 0.9044 9.39974 1.2868L9.40283 1.28856L14.4613 4.20761C15.1684 4.598 15.5746 5.33558 15.5746 6.11465V8.99996V11.8853C15.5746 12.6507 15.1632 13.3848 14.4617 13.7721L9.37973 16.7132C8.71029 17.0956 7.86428 17.0956 7.19485 16.7132L7.19088 16.7109L2.11279 13.772C1.40602 13.3816 1 12.6441 1 11.8653V8.98995V6.11465C1 5.31458 1.44381 4.59039 2.10827 4.21051L7.21487 1.2868Z"/></svg>'          
          },
          'serverSide': true,
          'serverMethod': 'post',
          'ajax': {
             'url':'<?= base_url("backend/mrp/empList")?>'
          },
          'columns': [
             { data: 'gih_tool_no'},
             { data: 'tooling_name'},
             { data: 'tooling_no'},
             { data: 'drawing_no'},
             { data: 'ownership'},
             { data: 'sampling'},
             { data: 'fail'},
             { data: 'passed'},
             { data: 'project_name'},
             { data: 'part_no'},
          ],
          'select': true,
          'select': 'single',
    });

$("#toggle-right").click(function(e, parameters) {
            var nonUI = true;
            try {
                nonUI = parameters.nonUI;
            } catch (e) {}
            var checked = this.checked;
        });

$('#empTable tbody').on('click', 'tr', function () {
        let data = table.row(this).data();

        let checkSideBarRight = document.getElementById('toggle-right').checked;

        if(!checkSideBarRight)
        {
          $("#toggle-right").trigger('click', {
            nonUI : true
          });
        }
        document.getElementById("gihToolNo").innerText= data.gih_tool_no;
        document.getElementById("toolingName").innerText= data.tooling_name;
        document.getElementById("toolingNo").innerText= data.tooling_no;
        document.getElementById("drawingNo").innerText= data.drawing_no;
        document.getElementById("ownerShip").innerText= data.ownership;
        document.getElementById("sampling").innerText= data.sampling;
        document.getElementById("fail").innerText= data.fail;
        document.getElementById("passed").innerText= data.passed;
        document.getElementById("project").innerText= data.project_name;
    });
}
$(document).ready(function(){
  displayDataTables()
});
</script>


