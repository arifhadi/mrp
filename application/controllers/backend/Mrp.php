<?php if (!defined('BASEPATH')) { exit ('No Direct Script Allowed'); }

class Mrp extends CI_Controller {

	public function __construct(){
		parent::__construct();
	    if(!$this->ion_auth->logged_in()){
	      redirect('auth/login', 'refresh');
	    }
	    $this->load->model('m_mrp');
	    $this->load->library('Pagination');
	    $this->load->library('pagination_mat');
	}

	// public function index()
	// {	
	// 	// log_r("aaa");
	// 	$this->admintemp->view('backend/dashboard');
	// }
	public function list_data_mrp()
	{

		$this->data['data'] = $this->m_mrp->get_data_list_mrp();
		// log_r($this->data['data']);
		$this->admintemp->view('backend/mrp/mrp_list',$this->data);
	}

	public function create()
	{
		$hariIni        = new DateTime();
        if (empty($this->input->post())) {

            $this->admintemp->view('backend/mrp/create_incoming_order');
         }

        elseif(!empty($this->input->post())) {
        $employee_no = $this->input->post('employee_no');
        $additional_cuti = $this->input->post('additional_cuti');

        $data = array(
            'employee_no'       => $employee_no,
            'additional_cuti'   => $additional_cuti,
            'create_at'         => $hariIni->format('y-m-d H:i:s'),
            'user_id'           => USER_ID,
        );

        $data_emp = array(
            'employee_no'   => $employee_no,
            'is_create'     => 1,
        );

        $status   = $this->m_annual_leave->create_additional_cuti($data);
        $status_emp = $this->m_annual_leave->update_employee($employee_no,$data_emp);
        $this->session->set_flashdata('success', 'Your data successfully Created !');
        redirect('backend/annual_leave/all_additional_cuti');

        
        }
	}

	public function create_part_details()
	{
		$hariIni        = new DateTime();
        if (empty($this->input->post())) {

            $this->admintemp->view('backend/mrp/create_part_details');
         }

        elseif(!empty($this->input->post())) {
        $employee_no = $this->input->post('employee_no');
        $additional_cuti = $this->input->post('additional_cuti');

        $data = array(
            'employee_no'       => $employee_no,
            'additional_cuti'   => $additional_cuti,
            'create_at'         => $hariIni->format('y-m-d H:i:s'),
            'user_id'           => USER_ID,
        );

        $data_emp = array(
            'employee_no'   => $employee_no,
            'is_create'     => 1,
        );

        $status   = $this->m_annual_leave->create_additional_cuti($data);
        $status_emp = $this->m_annual_leave->update_employee($employee_no,$data_emp);
        $this->session->set_flashdata('success', 'Your data successfully Created !');
        redirect('backend/annual_leave/all_additional_cuti');

        
        }
	}

	public function create_materials()
	{
		$hariIni        = new DateTime();
        if (empty($this->input->post())) {

        	$url 		= base_url().'backend/mrp/materials/1';
        	$config		= $this->pagination_mat->load($url,3);
			$this->pagination->initialize($config);
			$page 		= ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        	$str_links 	= $this->pagination->create_links();
        	$add 		= base_url().'backend/main_menu/material/'.'1'.'/'.'1';
        	$this->data["links"] = $str_links."&nbsp;<a href='".$add."'><span class='badge badge-pill badge-warning'></span></a>";
            $this->admintemp->view('backend/mrp/create_materials',$this->data);
         }

        elseif(!empty($this->input->post())) {
        $employee_no = $this->input->post('employee_no');
        $additional_cuti = $this->input->post('additional_cuti');

        $data = array(
            'employee_no'       => $employee_no,
            'additional_cuti'   => $additional_cuti,
            'create_at'         => $hariIni->format('y-m-d H:i:s'),
            'user_id'           => USER_ID,
        );

        $data_emp = array(
            'employee_no'   => $employee_no,
            'is_create'     => 1,
        );

        $status   = $this->m_annual_leave->create_additional_cuti($data);
        $status_emp = $this->m_annual_leave->update_employee($employee_no,$data_emp);
        $this->session->set_flashdata('success', 'Your data successfully Created !');
        redirect('backend/annual_leave/all_additional_cuti');

        
        }
	}

	public function materials($id='',$ids='')
	{

		$url 		= base_url().'backend/mrp/materials/'.$id;
        $config		= $this->pagination_mat->load($url,3);
		$this->pagination->initialize($config);
		$page 		= ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $str_links 	= $this->pagination->create_links();

        $add 		= base_url().'backend/main_menu/material/'.'1'.'/'.'1';
        $this->data["links"] = $str_links."&nbsp;<a href='".$add."'><span class='badge badge-pill badge-warning'></span></a>";
        if(!empty($ids))
        {
        	if($ids==2)
        	{
        		$this->admintemp->view('backend/mrp/create_masterbatch',$this->data);
        	}
        	else if($ids==3){
        		$this->admintemp->view('backend/mrp/create_other_materials',$this->data);
        	}
        }
        else{
        	$this->admintemp->view('backend/mrp/create_materials',$this->data);
        }
	}

	public function create_production_plan()
	{
		$hariIni        = new DateTime();
        if (empty($this->input->post())) {

        	$url 		= base_url().'backend/mrp/production_plan/1';
        	$config		= $this->pagination_mat->load($url,5);
			$this->pagination->initialize($config);
			$page 		= ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        	$str_links 	= $this->pagination->create_links();
        	$add 		= base_url().'backend/main_menu/production_plan/'.'1'.'/'.'1';
        	$this->data["links"] = $str_links."&nbsp;<a href='".$add."'><span class='badge badge-pill badge-warning'></span></a>";
            $this->admintemp->view('backend/mrp/create_plan_incoming',$this->data);
         }

        elseif(!empty($this->input->post())) {
        $employee_no = $this->input->post('employee_no');
        $additional_cuti = $this->input->post('additional_cuti');

        $data = array(
            'employee_no'       => $employee_no,
            'additional_cuti'   => $additional_cuti,
            'create_at'         => $hariIni->format('y-m-d H:i:s'),
            'user_id'           => USER_ID,
        );

        $data_emp = array(
            'employee_no'   => $employee_no,
            'is_create'     => 1,
        );

        $status   = $this->m_annual_leave->create_additional_cuti($data);
        $status_emp = $this->m_annual_leave->update_employee($employee_no,$data_emp);
        $this->session->set_flashdata('success', 'Your data successfully Created !');
        redirect('backend/annual_leave/all_additional_cuti');

        
        }
	}

	public function production_plan($id='',$ids='')
	{

		$url 		= base_url().'backend/mrp/production_plan/'.$id;
        $config		= $this->pagination_mat->load($url,5);
		$this->pagination->initialize($config);
		$page 		= ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $str_links 	= $this->pagination->create_links();

        $add 		= base_url().'backend/main_menu/material/'.'1'.'/'.'1';
        $this->data["links"] = $str_links."&nbsp;<a href='".$add."'><span class='badge badge-pill badge-warning'></span></a>";
        if(!empty($ids))
        {
        	if($ids==2)
        	{
        		$this->admintemp->view('backend/mrp/create_plan_production',$this->data);
        	}
        	else if($ids==3){
        		$this->admintemp->view('backend/mrp/create_plan_quality',$this->data);
        	}
        	else if($ids==4){
        		$this->admintemp->view('backend/mrp/create_plan_werehouse',$this->data);
        	}
        	else if($ids==5){
        		$this->admintemp->view('backend/mrp/create_plan_shipping',$this->data);
        	}
        }
        else{
        	$this->admintemp->view('backend/mrp/create_plan_incoming',$this->data);
        }
	}

    public function empList()
    {
         // POST data
     $postData = $this->input->post();

     // Get data
     $data = $this->m_mrp->getEmployees($postData);

     echo json_encode($data);
    }

    public function filter_data_mrp()
    {
        $gih_tool_no = $this->input->post('gih_tool_no');
        $tooling_name = $this->input->post('tooling_name');
        $tooling_no = $this->input->post('tooling_no');


        $data = $this->m_mrp->get_filterdata_mrp($gih_tool_no,$tooling_name,$tooling_no);

        echo json_encode($data);


    }



}
