<?php if (!defined('BASEPATH')) { exit ('No Direct Script Allowed'); }

class Annual_leave extends CI_Controller {

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('m_annual_leave');
	}

    public function create_annual_leave_emp()
    {
        $hariIni        = new DateTime();
        if (empty($this->input->post())) {

			$this->usertemp->view('annual_leave/request_annual_leave');

        }elseif (!empty($this->input->post())) {
            $employee_no                = $this->input->post('employee_no');
            $category_annual     = $this->input->post('category_annual');
            $from_leave                 = $this->input->post('from_leave');
            $to_leave            = $this->input->post('to_leave');
            $total_leave          = $this->input->post('total_leave');
            $reason            = $this->input->post('reason');
            $from_leave_nor = $this->input->post('from_leave_nor');
            $to_leave_nor = $this->input->post('to_leave_nor');

            $format_al =  $hariIni->format('d-m-y');
            $digit_al = substr($format_al, -2);
            $digit_emp_no = substr($employee_no, -2);
            $random_str = $this->m_annual_leave->randomString(3);
            $code_al = $random_str.$digit_emp_no.$digit_al;

             $list_employee = $this->m_annual_leave->cek_employee($employee_no);
             $name_emp = $list_employee['name_emp'];
             $date_now = $hariIni->format('Y-m-d');
             $doj = $list_employee['date_join_contract'];
             $date2=date_create($date_now);
             $date1=date_create($doj);
             $diff = date_diff($date1,$date2);
             // $date_emp_al = $diff->format("%R%a");
             $date_emp_al = $diff->days;

             // log_r($total_leave);

             $cek_count_ttl_leave = $this->m_annual_leave->get_count_ttl_al_emp($employee_no);
             // log_r($cek_count_ttl_leave);

            if(!empty($cek_count_ttl_leave))
            {
                 //First Code Hitung Jumlah Cuti yang di dapat Selama kerja 
                    $date_now = $hariIni->format('Y-m-d');

                    $date_no_create = date_create($date_now);
                    $date1= date_create($cek_count_ttl_leave->doj);

                    $jarak = date_diff($date_no_create,$date1);

                    $date2=date_create($date_now);
                    //Logic Menghitung DOJ dan Date Bulan 4 itu Selama Berapa Hari Range nya

                    
                  
                    $diff = date_diff($date1,$date2);
                    $diff_date = $diff->days;
                    $date_count = floor($diff_date / 30);
                    $sub_str = substr($date_now, 5,2);

                    $year_now = $hariIni->format('Y');
                    
                    $year_convert_2 = date('Y',$year_now);
                    $year_convert = $year_now -1 ;
                   
                    

                    if($sub_str==04 || $sub_str >04)
                    {
                        if($date_count >=5)
                        {
                           if($sub_str ==04)
                           {
                              $date_count = 3;
                           }
                           
                           if($sub_str > 04)
                           {
                              $date_first = $year_convert_2."-"."01"."-"."01"; // tanggal dari bulan 1 dari tahun sekarang
                              $date_first_convert = date_create($date_first);
                              $date_diff_first = date_diff($date2,$date_first_convert);
                              $date_count_first = $date_diff_first->days;
                              $date_count = floor($date_count_first / 30);
                           }
                        }

                        if($jarak->days >= 122 && $jarak->days <= 151 )
                        {
                          $date_count = $date_count;
                        }
                        
                    }
                    if($diff_date >=365)
                    {
                        if($sub_str >= 01 && $sub_str<=03)
                        {
                          $date_reset = $year_convert."-"."04"."-"."01";
                          $date_old_2 = date_create($date_reset);
                          $date_old_1 = date_create($date_range);
                          $diff_old_date = date_diff($date_old_1,$date_old_2);
                          $diff_count_old = $diff_old_date->days;
                          $date_count = floor($diff_count_old / 30 + 3);

                        }
                    }

                    $balance_cuti = $date_count - $cek_count_ttl_leave->ttl_leave; //Total Pengurangan Jumlah Cuti Yang di ajukan dan Jumlah sisa/saldo/jatah cuti 

                    //End Hitung
                    if($balance_cuti >0)
                    {
                        if($total_leave > $balance_cuti)
                        {
                            //Cek karyawan permanen atau tidak
                            if($cek_count_ttl_leave->eoc =='0000-00-00')
                            {
                                //first
                                 $is_approve_spv = 0;

                                if(isset($list_employee))
                                {
                                    if($list_employee['department'] =="PRODUCTION" or $list_employee['department'] =="WAREHOUSE" or $list_employee['department'] =="TOOLING" or $list_employee['department'] =="FACILITY" or $list_employee['department'] =="MAINTENANCE"or $list_employee['department'] =="LOGISTIC")
                                    {
                                        $user_supervisor = 16;
                                        $user_hod =18;
                                        $user_hrd =3;
                                    }
                                    if($list_employee['department'] =="QA/QC")
                                    {
                                        $user_supervisor = 17;
                                        $user_hod =18;
                                        $user_hrd =3;
                                    }
                                    else{

                                        $user_supervisor = 15;
                                        $user_hod =15;
                                        $user_hrd =3;
                                        $is_approve_spv = 1;
                                    }
                                }

                                if(empty($from_leave))
                                {
                                    $from_leave = $from_leave_nor;
                                    $to_leave = $to_leave_nor;
                                };

                        $data = array(
                            'employee_no'               => $employee_no,
                            'code_al'                   => $code_al,
                            'status'                    => $category_annual,
                            'from_leave'                => $from_leave,
                            'to_leave'                  => $to_leave,
                            'total_leave'               => $total_leave,
                            'reason'                    => $reason,
                            'user_supervisor'           => $user_supervisor,
                            'user_hod'                  => $user_hod,
                            'user_hrd'                  => $user_hrd,
                            'create_at'                 => $hariIni->format('y-m-d H:i:s'),
                        );

                        $data_al = array(
                            'code_al'   => $code_al,
                            'is_approve_spv' => $is_approve_spv,
                        );

                        $status = $this->m_annual_leave->insert_annual_leave($data);
                        $status_detail_al = $this->m_annual_leave->insert_detail_annual_leave($data_al);
                        if ($status == 1) {//Jika Success Insert
                            $this->finish_request_annual($employee_no,$name_emp,$code_al);
                
                        if($is_approve_spv ==0)
                        {
                            $approve_to = "SUPERVISOR";
                            $this->send_create_to_spv($code_al,$user_supervisor,$employee_no,$approve_to);
                        }
                        else{
                            $approve_to = "HOD";
                            $this->send_create_to_spv($code_al,$user_hod,$employee_no,$approve_to);
                        }
                        }else if($status == 'error'){
                            $this->session->set_flashdata('error', 'Code No is available, please make a unique one !');
                            redirect('frontend/annual_leave/create_annual_leave_emp');
                        }
                                //end
                            }
                            else{
                                $this->session->set_flashdata('error', 'failed sorry your request annual leave is exceed the remaining leave available ');
                                redirect('frontend/annual_leave/create_annual_leave_emp');
                            }
                            //End Cek karyawan permanen
                        }
                        else{
                                 $is_approve_spv = 0;

                                if(isset($list_employee))
                                {
                                    if($list_employee['department'] =="PRODUCTION" or $list_employee['department'] =="WAREHOUSE" or $list_employee['department'] =="TOOLING" or $list_employee['department'] =="FACILITY" or $list_employee['department'] =="MAINTENANCE"or $list_employee['department'] =="LOGISTIC")
                                    {
                                        $user_supervisor = 16;
                                        $user_hod =18;
                                        $user_hrd =3;
                                    }
                                    if($list_employee['department'] =="QA/QC")
                                    {
                                        $user_supervisor = 17;
                                        $user_hod =18;
                                        $user_hrd =3;
                                    }
                                    else{

                                        $user_supervisor = 15;
                                        $user_hod =15;
                                        $user_hrd =3;
                                        $is_approve_spv = 1;
                                    }
                                }

                                if(empty($from_leave))
                                {
                                    $from_leave = $from_leave_nor;
                                    $to_leave = $to_leave_nor;
                                };

                        $data = array(
                            'employee_no'               => $employee_no,
                            'code_al'                   => $code_al,
                            'status'                    => $category_annual,
                            'from_leave'                => $from_leave,
                            'to_leave'                  => $to_leave,
                            'total_leave'               => $total_leave,
                            'reason'                    => $reason,
                            'user_supervisor'           => $user_supervisor,
                            'user_hod'                  => $user_hod,
                            'user_hrd'                  => $user_hrd,
                            'create_at'                 => $hariIni->format('y-m-d H:i:s'),
                        );

                        $data_al = array(
                            'code_al'   => $code_al,
                            'is_approve_spv' => $is_approve_spv,
                        );

                        $status = $this->m_annual_leave->insert_annual_leave($data);
                        $status_detail_al = $this->m_annual_leave->insert_detail_annual_leave($data_al);
                        if ($status == 1) {//Jika Success Insert
                            $this->finish_request_annual($employee_no,$name_emp,$code_al);
                
                        if($is_approve_spv ==0)
                        {
                            $approve_to = "SUPERVISOR";
                            $this->send_create_to_spv($code_al,$user_supervisor,$employee_no,$approve_to);
                        }
                        else{
                            $approve_to = "HOD";
                            $this->send_create_to_spv($code_al,$user_hod,$employee_no,$approve_to);
                        }
                        }else if($status == 'error'){
                            $this->session->set_flashdata('error', 'Code No is available, please make a unique one !');
                            redirect('frontend/annual_leave/create_annual_leave_emp');
                        }
                        }
                    }
                    else if($balance_cuti <= 0)
                    {
                        //first
                           if($cek_count_ttl_leave->eoc =='0000-00-00')
                            {
                                //first
                                 $is_approve_spv = 0;

                                if(isset($list_employee))
                                {
                                    if($list_employee['department'] =="PRODUCTION" or $list_employee['department'] =="WAREHOUSE" or $list_employee['department'] =="TOOLING" or $list_employee['department'] =="FACILITY" or $list_employee['department'] =="MAINTENANCE"or $list_employee['department'] =="LOGISTIC")
                                    {
                                        $user_supervisor = 16;
                                        $user_hod =18;
                                        $user_hrd =3;
                                    }
                                    if($list_employee['department'] =="QA/QC")
                                    {
                                        $user_supervisor = 17;
                                        $user_hod =18;
                                        $user_hrd =3;
                                    }
                                    else{

                                        $user_supervisor = 15;
                                        $user_hod =15;
                                        $user_hrd =3;
                                        $is_approve_spv = 1;
                                    }
                                }

                                if(empty($from_leave))
                                {
                                    $from_leave = $from_leave_nor;
                                    $to_leave = $to_leave_nor;
                                };

                        $data = array(
                            'employee_no'               => $employee_no,
                            'code_al'                   => $code_al,
                            'status'                    => $category_annual,
                            'from_leave'                => $from_leave,
                            'to_leave'                  => $to_leave,
                            'total_leave'               => $total_leave,
                            'reason'                    => $reason,
                            'user_supervisor'           => $user_supervisor,
                            'user_hod'                  => $user_hod,
                            'user_hrd'                  => $user_hrd,
                            'create_at'                 => $hariIni->format('y-m-d H:i:s'),
                        );

                        $data_al = array(
                            'code_al'   => $code_al,
                            'is_approve_spv' => $is_approve_spv,
                        );

                        $status = $this->m_annual_leave->insert_annual_leave($data);
                        $status_detail_al = $this->m_annual_leave->insert_detail_annual_leave($data_al);
                        if ($status == 1) {//Jika Success Insert
                            $this->finish_request_annual($employee_no,$name_emp,$code_al);
                
                        if($is_approve_spv ==0)
                        {
                            $approve_to = "SUPERVISOR";
                            $this->send_create_to_spv($code_al,$user_supervisor,$employee_no,$approve_to);
                        }
                        else{
                            $approve_to = "HOD";
                            $this->send_create_to_spv($code_al,$user_hod,$employee_no,$approve_to);
                        }
                        }else if($status == 'error'){
                            $this->session->set_flashdata('error', 'Code No is available, please make a unique one !');
                            redirect('frontend/annual_leave/create_annual_leave_emp');
                        }
                                //end
                            }
                            else{
                                $this->session->set_flashdata('error', 'failed sorry your request annual leave is exceed the remaining leave available ');
                                redirect('frontend/annual_leave/create_annual_leave_emp');
                            }
                            //End Cek karyawan permanen
                        //end
                    }
             
            }

            else{
                if($date_emp_al < 120)
             {
                $this->session->set_flashdata('error', 'failed because your contract is not more than 4 months');
                 redirect('frontend/annual_leave/create_annual_leave_emp');
             }

             else {

             $is_approve_spv = 0;

             if(isset($list_employee))
             {
                if($list_employee['department'] =="PRODUCTION" or $list_employee['department'] =="WAREHOUSE" or $list_employee['department'] =="TOOLING" or $list_employee['department'] =="FACILITY" or $list_employee['department'] =="MAINTENANCE"or $list_employee['department'] =="LOGISTIC")
                {
                    $user_supervisor = 16;
                    $user_hod =18;
                    $user_hrd =3;
                }
                if($list_employee['department'] =="QA/QC")
                {
                    $user_supervisor = 17;
                    $user_hod =18;
                    $user_hrd =3;
                }
                else{

                    $user_supervisor = 15;
                    $user_hod =15;
                    $user_hrd =3;
                    $is_approve_spv = 1;
                }
             }

             if(empty($from_leave))
             {
                $from_leave = $from_leave_nor;
                $to_leave = $to_leave_nor;
             };

            $data = array(
                'employee_no'               => $employee_no,
                'code_al'                   => $code_al,
                'status'                    => $category_annual,
                'from_leave'                => $from_leave,
                'to_leave'                  => $to_leave,
                'total_leave'               => $total_leave,
                'reason'                    => $reason,
                'user_supervisor'           => $user_supervisor,
                'user_hod'                  => $user_hod,
                'user_hrd'                  => $user_hrd,
                'create_at'                 => $hariIni->format('y-m-d H:i:s'),
            );

            $data_al = array(
                'code_al'   => $code_al,
                'is_approve_spv' => $is_approve_spv,
            );

            $status = $this->m_annual_leave->insert_annual_leave($data);
            $status_detail_al = $this->m_annual_leave->insert_detail_annual_leave($data_al);
            if ($status == 1) {//Jika Success Insert
                // $this->session->set_flashdata('success', 'Your Request successfully added !');
                $this->finish_request_annual($employee_no,$name_emp,$code_al);
                
                if($is_approve_spv ==0)
                {
                    $approve_to = "SUPERVISOR";
                    $this->send_create_to_spv($code_al,$user_supervisor,$employee_no,$approve_to);
                }
                else{
                    $approve_to = "HOD";
                    $this->send_create_to_spv($code_al,$user_hod,$employee_no,$approve_to);
                }
            }else if($status == 'error'){
                $this->session->set_flashdata('error', 'Code No is available, please make a unique one !');
                redirect('frontend/annual_leave/create_annual_leave_emp');
            }
        }
    }
 


        }

    }

    public function finish_request_annual($employee_no='',$name_emp='',$code_al='')
    {
        $this->data['employee_no'] = $employee_no;
        $this->data['name_emp'] = $name_emp;
        $this->data['code_al'] = $code_al;
        $this->usertemp->view('annual_leave/finish_annual_request',$this->data);

    }

    public function check_request($code_al='')
    {
        if(empty($code_al))
        {
            $this->usertemp->view('annual_leave/list_check');
        }
        else{
             $get_detail_al = $this->m_annual_leave->get_check_al($code_al);
            if(empty($get_detail_al))
            {
                $this->session->set_flashdata('error', 'Wrong Code Or Data Not Available !');
                $this->usertemp->view('annual_leave/list_check');

            }
            else{

             $this->data['data_al'] = $get_detail_al;
             $this->data['reason_reject_spv'] = $get_detail_al[0]->reason_reject_spv;
             $this->data['reason_reject_hod'] = $get_detail_al[0]->reason_reject_hod;
             $this->data['reason_reject_hrd'] = $get_detail_al[0]->reason_reject_hrd;
             $this->usertemp->view('annual_leave/list_check_al',$this->data);
            }
        }
    }


    function send_create_to_spv($code_al='', $user_supervisor='',$employee_no='',$approve_to='')
    {
        $this->load->library('mailer');

        $get_req   = $this->m_annual_leave->get_user_req($employee_no);//get requestor id
        $email_req = $get_req->email;
        $this->data['name_req']  =  $get_req->name;

        $get_to   = $this->m_annual_leave->get_user_row($user_supervisor);//get to id "SPV"
        $email_to = $get_to->email;
        $this->data['name_to']  =  $get_to->first_name;
        $this->data['approve_to'] = $approve_to;
        
        $get_detail_al = $this->m_annual_leave->get_check_al($code_al);
      
      
        $subjek                     = "Leave Form Request"." ".$code_al;
        $this->data['code_al']    = $code_al;
        $this->data['department']    = $get_req->department;
        $this->data['employee_no']     =$employee_no;
        $this->data['date_request']     =$get_detail_al[0]->create_at;
        $this->data['status']   = $get_detail_al[0]->status;
        $this->data['from_leave']        = $get_detail_al[0]->from_leave;
        $this->data['to_leave']        = $get_detail_al[0]->to_leave;
        $this->data['total_leave']    = $get_detail_al[0]->total_leave;
        $this->data['reason']     = $get_detail_al[0]->reason;
        
    
        $content = $this->load->view('annual_leave/mail_create_al_spv', $this->data, true);//view email
       

        $mailto = array(
            'penerima_satu'  => $email_to,
        );



        $ccmail = array(
            $cc_satu  = 'noreply@asiagalaxy.com',
        );

        $sendmail = array(
            'email_penerima' => "",
            'subjek'  => $subjek,
            'content' => $content,
        );

        $send = $this->mailer->send($sendmail, $mailto, $ccmail); 
        $status = $send['status'];
        if ($status == 'Sukses') {
            $this->session->set_flashdata('success', 'Sending email was successful !');
        }elseif($status == 'Gagal'){
            $this->session->set_flashdata('error', 'Sending email failed !');
        }
    }

    public function getEmployeeDetail()
    {
    	$employee_no = $this->input->post('id');
    	$data = $this->m_annual_leave->cek_employee($employee_no);

    	echo json_encode($data);
    }
	
}
