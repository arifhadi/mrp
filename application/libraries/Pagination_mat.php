<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pagination_mat {
        /*var $template_data = array();
	
		function set($name, $value)
		{
			$this->template_data[$name] = $value;
		}*/
	
	function load($url,$total_row)
	{       
                $config["base_url"]         = $url;
                $config["total_rows"]       = $total_row;
                $config["per_page"]         = 1;
                $config["uri_segment"]      = 5;  // uri parameter
                $choice = $config["total_rows"] / $config["per_page"];
                $config['use_page_numbers'] = TRUE;
                $config['num_links']        = 1;     
	       // Membuat Style pagination untuk BootStrap v4
                $config['first_link']       = '<i class="icon md-skip-previous" aria-hidden="true"></i>';
                $config['last_link']        = '<i class="icon md-skip-next" aria-hidden="true"></i>';
                $config['next_link']        = '<i class="icon md-chevron-right" aria-hidden="true"></i>';
                $config['prev_link']        = '<i class="icon md-chevron-left" aria-hidden="true"></i>';
                $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
                $config['full_tag_close']   = '</ul></nav></div>';
                $config['num_tag_open']     = '<li class=""><span class="badge badge-pill badge-secondary">';
                $config['num_tag_close']    = '</span></li>';
                $config['cur_tag_open']     = '<li class=" active"><span class="badge badge-pill badge-danger">';
                $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
                $config['next_tag_open']    = '<li class=""><span class="badge badge-pill badge-info">';
                $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
                $config['prev_tag_open']    = '<li class=""><span class="badge badge-pill badge-info">';
                $config['prev_tagl_close']  = '</span>Next</li>';
                $config['first_tag_open']   = '<li class=""><span class="badge badge-pill badge-info">';
                $config['first_tagl_close'] = '</span></li>';
                $config['last_tag_open']    = '<li class=""><span class="badge badge-pill badge-info">';
                $config['last_tagl_close']  = '</span></li>';
         
                return $config;
        }
}

/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */