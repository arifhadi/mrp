<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model Register
 */
class M_mrp extends CI_Model{

	// private $table1 = 'u_user_register';
	// private $table2 = 'users';

	public function __construct()
	{
		$this->db2  = $this->load->database('tooling', true);
		$this->db3  = $this->load->database('productionactivity', true);

		// log_r($this->db2);
	}

	public function get_data_list_mrp()
	{
		// $this->db2  = $this->load->database('tooling', true);

		// log_r($this->db2);
		// $this->db2->select('a.*,b.*,c.*');
		// $this->db2->from('mold_basic_details as a');
		// $this->db2->join('productionactivity.ijm_material_part as b','b.part_name = a.tooling_name','left');
		// $this->db2->join('productionactivity.p_masterdata as c','c.part_name = a.gih_tool_no','right');
		// $this->db2->where_in('a.status',[1,4]);
		// $this->db2->group_by('id_group_master');
		// return $this->db2->get()->result(); 
		// log_r($this->db3);
		$db_hostname_tooling = $this->db2->database;
		$db_hostname_productionactivity = $this->db3->database;
		$sql = "SELECT a.*,b.*,c.* FROM $db_hostname_tooling.mold_basic_details as a LEFT JOIN $db_hostname_productionactivity.ijm_material_part as b ON b.part_name = a.tooling_name RIGHT JOIN $db_hostname_productionactivity.p_masterdata as c ON c.part_name = a.gih_tool_no  WHERE a.`status` IN ('1,4') GROUP BY c.id_group_master";
		$query = $this->db->query($sql);
		return $query->result_object();

	}

	function getEmployees($postData=null){

     $response = array();

     ## Read value
     $draw = $postData['draw'];
     $start = $postData['start'];
     $rowperpage = $postData['length']; // Rows display per page
     // log_r($start);
     $columnIndex = $postData['order'][0]['column']; // Column index
     $columnName = $postData['columns'][$columnIndex]['data']; // Column name
     $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
     $searchValue = $postData['search']['value']; // Search value
     // log_r($searchValue);
     ## Search 
     $searchQuery = "";
     if($searchValue != ''){
        $searchQuery = " (gih_tool_no like '%".$searchValue."%' or tooling_name like '%".$searchValue."%' or drawing_no like'%".$searchValue."%' or drawing_no like '%".$searchValue."%' or ownership like '%".$searchValue."%' or sampling like '%".$searchValue."%' or fail like '%".$searchValue."%' or passed like '%".$searchValue."%' or project_name like '%".$searchValue."%' or part_no like '%".$searchValue."%' ) ";
     }

     ## Total number of records without filtering
     // $this->db->select('count(*) as allcount');
     // $records = $this->db->get('employees')->result();
     $db_hostname_tooling = $this->db2->database;
	 $db_hostname_productionactivity = $this->db3->database;
	 // $sql = "SELECT  COUNT(*) OVER () as ttl FROM $db_hostname_tooling.mold_basic_details as a LEFT JOIN $db_hostname_productionactivity.ijm_material_part as b ON b.part_name = a.tooling_name RIGHT JOIN $db_hostname_productionactivity.p_masterdata as c ON c.part_name = a.gih_tool_no  WHERE a.`status` IN ('1,4') GROUP BY c.id_group_master LIMIT 1" ;
	 // $query = $this->db->query($sql);
	 // $records =  $query->result_object();
	 $this->db2->select('COUNT(*) OVER () as ttl');
	 $this->db2->from($db_hostname_tooling.'.mold_basic_details as a');
	 $this->db2->join($db_hostname_productionactivity.'.ijm_material_part as b','b.part_name = a.tooling_name','left');
	 $this->db2->join($db_hostname_productionactivity.'.p_masterdata as c','c.part_name = a.gih_tool_no','left');
     $this->db2->where('a.status',1);
     $this->db2->group_by('c.id_group_master');
	 $this->db2->limit(1);
	 $records = $this->db2->get()->result(); 
	 // log_r($totalRecords);
     $totalRecords = $records[0]->ttl;
	 // log_r($totalRecords);


     ## Total number of record with filtering
     $this->db2->select('COUNT(*) OVER () as ggs');
     if($searchQuery != '')
        $this->db2->where($searchQuery);
        $this->db2->where('a.status',1);
        $this->db2->from($db_hostname_tooling.'.mold_basic_details as a');
	 	$this->db2->join($db_hostname_productionactivity.'.ijm_material_part as b','b.part_name = a.tooling_name','left');
	 	$this->db2->join($db_hostname_productionactivity.'.p_masterdata as c','c.part_name = a.gih_tool_no','left');
        $this->db2->group_by('c.id_group_master');
	 	$this->db2->limit(1);
     $records = $this->db2->get()->result();
     // log_r($records[0]->ggs);
     $totalRecordwithFilter = $records[0]->ggs;
     // log_r($totalRecordwithFilter);

     // log_r($totalRecordwithFilter);

     ## Fetch records
     $this->db2->select('a.*,b.*,c.*');
     if($searchQuery != '')
     	$this->db2->where($searchQuery);
        $this->db2->where('a.status',1);
        $this->db2->from($db_hostname_tooling.'.mold_basic_details as a');
	 	$this->db2->join($db_hostname_productionactivity.'.ijm_material_part as b','b.part_name = a.tooling_name','left');
	 	$this->db2->join($db_hostname_productionactivity.'.p_masterdata as c','c.part_name = a.gih_tool_no','left');
        $this->db2->group_by('c.id_group_master');
        $this->db2->order_by($columnName, $columnSortOrder);
        $this->db2->limit($rowperpage, $start);
        $records = $this->db2->get()->result();

     $data = array();

     foreach($records as $record ){

        $data[] = array( 
           "gih_tool_no"=>$record->gih_tool_no,
           "tooling_name"=>$record->tooling_name,
           "tooling_no"=>$record->tooling_no,
           "drawing_no"=>$record->drawing_no,
           "ownership"=>$record->ownership,
           "sampling"=>$record->sampling,
           "fail"=>$record->fail,
           "passed"=>$record->passed,
           "project_name"=>$record->project_name,
           "part_no"=>$record->part_no,
        ); 
     }

     ## Response
     $response = array(
        "draw" => intval($draw),
        "recordsTotal" => $totalRecords,
        "recordsFiltered" => $totalRecordwithFilter,
        "data" => $data
     );

     return $response; 
   }

   function get_filterdata_mrp($gih_tool_no='',$tooling_name='',$tooling_no='')
   {
        $db_hostname_tooling = $this->db2->database;
        $db_hostname_productionactivity = $this->db3->database;
        $v = 'WIK-TL-0173';
        $a = 'THERMO';
        $b = '9770.12-033';
        $searchQuery = "a.gih_tool_no like '%".$gih_tool_no."%' or a.tooling_name like '%".$tooling_name."%' or a.tooling_no like'%".$tooling_no."%'";
        // log_r($searchQuery);
        $this->db2->select('a.*,b.*,c.*');
        $this->db2->from($db_hostname_tooling.'.mold_basic_details as a');
        $this->db2->join($db_hostname_productionactivity.'.ijm_material_part as b','b.part_name = a.tooling_name','left');
        $this->db2->join($db_hostname_productionactivity.'.p_masterdata as c','c.part_name = a.gih_tool_no','right');
        $this->db2->where('a.status',1);
        $this->db2->where($searchQuery);
        $this->db2->group_by('c.id_group_master');
        return $this->db2->get()->result();
   }
}


?>